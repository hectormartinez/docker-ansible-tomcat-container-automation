# Tomcat Docker container automation

The code in this repository aims to automate a [sample web application](https://tomcat.apache.org/tomcat-9.0-doc/appdev/sample/) for [Apache Tomcat 9.0](https://tomcat.apache.org/download-90.cgi) using [Ansible](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html) and [Docker](https://docs.docker.com/), with some tests written with [pytest](https://docs.pytest.org/en/latest/) a Python test framework.

This repository expects you to have the following installed on your machine:

* Python 3
* Docker
* Ansible
* Pytest

**Note**: I' using Windows.

My current workspace is Windows 10 where I have installed Vagrant and Virtualbox. I will be using it to spin up a debian buster machine to work in a Linux environment. If you are working within a Linux you do not need this.

When you start the VM with `vagrant up` it will run some scripts and install the expected dependencies listed at the start of this document. From there you can SSH into the machine, move to the `/vagrant` folder and follow the instructions.

```
vagrant up
```

## Instructions

**Note:** be warned this process will install Python dependencies in your Python user space

0. Ensure you have the prerequisites. You can run these commands and ensure they exit properly. Make sure also that you are using the latest versions of them.
    1. Run `docker run hello-world`
    2. Run `ansible --version`
    3. Run `pytest -V`
1. Clone this repostory.
2. `cd` into it.
3. Run `ansible-inventory -i inventory playbook.yml` to run it.
4. Visit `http://localhost:8080/sample/` in your machine.

The playbook is idempotent and the container is not replaced if the sha256 of the built container is the same, so no need to check if there is already running. If something is running but the container changes, it will be replaced.

## Possible improvements

* This is using the Docker image `tomcat:9` as base which could not be as secure as required. The option is to scan the image to ensure there are no security issues or build the image from a secured base image and then downloading and setting up Tomcat via binaries, which will need less or no dependencies.
* The playbook could build locally, push the image to a registry and then launch the container in a remote host instead of localhost.
