import pytest
import requests
from urllib3.util import Retry


@pytest.fixture
def retry_requests():
    session = requests.Session()
    retry = Retry(status_forcelist=[404, 413, 429, 503], backoff_factor=0.3)
    adapter = requests.adapters.HTTPAdapter(max_retries=retry)
    session.mount("http://", adapter)
    return session


def test_application_listening(retry_requests):
    response = retry_requests.get('http://localhost:8080/sample/')
    assert response.status_code != 404
