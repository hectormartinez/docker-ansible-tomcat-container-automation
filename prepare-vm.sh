# Make bash stop if there is some error and to print commands as they are executed
set -ex

# Install Docker as per the official documentation
sudo apt-get update
sudo apt-get install -y -qq \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
    "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y -qq docker-ce docker-ce-cli containerd.io

# Add user to docker group so it can use docker without sudo
sudo usermod -aG docker $USER

# Install python3-pip
sudo apt-get -y -qq install python3-pip

# TODO: use virtualenv instead
# Install pytest and ansible for the current user
python3 -m pip install pytest ansible --user

# Add user bin path to PATH so it can use `ansible` and `pytest`
echo 'export PATH="$HOME/.local/bin:$PATH"' | tee -a $HOME/.bashrc
